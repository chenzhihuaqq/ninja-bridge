export class util{
    private static _instance:util;

    public static instance():util{
        if(!this._instance) this._instance = new util()
        return this._instance
    }

    public maxX:number = 300       // 桥与桥间最大距离
    public minx:number = 100       // 桥与桥间最小距离

    public maxW:number = 150        //桥梁最大的宽度
    public minW:number = 30        //桥梁最小的宽度



    // 背景自适应
    public bgResize(node:cc.Node){
         // 1. 先找到 SHOW_ALL 模式适配之后，本节点的实际宽高以及初始缩放值
        let srcScaleForShowAll = Math.min(cc.view.getCanvasSize().width / node.width, cc.view.getCanvasSize().height / node.height);
        let realWidth = node.width * srcScaleForShowAll;
        let realHeight = node.height * srcScaleForShowAll;

        // 2. 基于第一步的数据，再做缩放适配
        node.scale = Math.max(cc.view.getCanvasSize().width / realWidth, cc.view.getCanvasSize().height / realHeight);
    }

    // 内容自适应
    public contentResize(node:cc.Node){
          // 1. 先找到 SHOW_ALL 模式适配之后，本节点的实际宽高以及初始缩放值
        let srcScaleForShowAll = Math.min(cc.view.getCanvasSize().width / node.width, cc.view.getCanvasSize().height / node.height);
        let realWidth = node.width * srcScaleForShowAll;
        let realHeight = node.height * srcScaleForShowAll;

        // 2. 基于第一步的数据，再做节点宽高适配
        node.width = node.width * (cc.view.getCanvasSize().width / realWidth);
        node.height = node.height * (cc.view.getCanvasSize().height / realHeight);
    }

    // 随机范围
    public randomNum(max:number,min:number){
        return Math.random()*(max - min) + min
    }
}