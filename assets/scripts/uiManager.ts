import gameManager from "./gameManager";
import { util } from "./util";

const {ccclass, property} = cc._decorator;

@ccclass
export default class uiManager extends cc.Component {

    @property(cc.Prefab)
    birdgePre:cc.Prefab = null;
    @property(cc.NodePool)
    birdgePool:cc.NodePool = null;
    @property(cc.Node)
    ninja:cc.Node = null;

    private game:gameManager = null;
    private defaultVec2:cc.Vec2 = null;

    private isInitStatus:boolean = false;

   init(game:gameManager){
        this.game = game
        util.instance().contentResize(this.node)
        this.initScene()
        this.node.on('touchstart',this.touchStart,this)
        this.node.on('touchend',this.touchEnd,this)
        this.node.on('touchcancel',this.touchEnd,this)

   }

   private touchStart(){
       if(!this.isInitStatus)return

   }

   private touchEnd(){
    
   }

   private initScene(){
    this.initBirdgePool()
   }

   private initBirdgePool(){
       this.birdgePool = new cc.NodePool()
       for(let i = 0;i<10;i++){
            let node = cc.instantiate(this.birdgePre)
             this.birdgePool.put(node)
       }
       this.BirdgeNode()
   }

   private BirdgeNode(){
       let node:cc.Node = null
       if(this.birdgePool.size()>0){
           node = this.birdgePool.get()
       }else{
           node = cc.instantiate(this.birdgePre)
       }
       node.getComponent('birdge').init(this)
       if(!this.isInitStatus){
           node.getChildByName('top').getChildByName('bonus').active = false
       }
       node.parent = this.node
   }

   public setBirdgePos(node:cc.Node){
       if(!this.isInitStatus){
        node.y = -cc.winSize.height/2 + node.height - 23 //23为桥梁头部高度
       node.x = -(cc.winSize.width/2 - node.width/2)
       this.ninja.x = node.x
       this.ninja.y = node.y + this.ninja.height/2 + 15 //15为微调距离
       this.defaultVec2 = new cc.Vec2(this.node.x,this.node.y)
       }
        
   }

    // update (dt) {}
}
