import uiManager from "./uiManager";
import { util } from "./util";

const {ccclass, property} = cc._decorator;

@ccclass
export default class gameManager extends cc.Component {

    @property(uiManager)
    uiManager:uiManager = null;

    onLoad () {
        util.instance().contentResize(this.node)
        this.uiManager.init(this)
    }

    start () {

    }

    // update (dt) {}
}
