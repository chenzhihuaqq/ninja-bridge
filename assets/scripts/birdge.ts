import uiManager from "./uiManager";
import { util } from "./util";

const {ccclass, property} = cc._decorator;

@ccclass
export default class birdge extends cc.Component {


    private top:cc.Node = null;
    private bottom:cc.Node = null;
    private uiManager:uiManager = null;
    private firstSet:boolean = false;
    onLoad () {
        this.top = this.node.getChildByName('top')
        this.bottom = this.node.getChildByName('bottom')
        let num = util.instance().randomNum(util.instance().maxW,util.instance().minW)
        this.node.width = this.top.width = this.bottom.width = num
        if(this.uiManager){
            this.uiManager.setBirdgePos(this.node)
        }
    }

    init(uiManager:uiManager){
        this.uiManager = uiManager
    }

}
